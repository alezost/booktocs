Contents 6
Preface 14

Chapter 0. About Euclidean Geometry 18
 §0.1. The axioms of Euclidean plane geometry 19
 §0.2. Commentary 22
 §0.3. Rotations 24
 §0.4. Parallel translations and vectors 28
 §0.5. Triangles: congruence, properties 30
 §0.6. Homothety and similitude 32
 §0.7. Angle measure and trigonometry 35
 §0.8. Properties of the circle 37
 §0.9. Isometries of the plane 41
 §0.10. Space geometry 45

Chapter 1. Toy Geometries and Main Definitions 50
 §1.1. Isometries of the Euclidean plane and space 50
 §1.2. Symmetries of some figures 52
 §1.3. Transformation groups 58
 §1.4. The category of geometries 63
 §1.5. Some philosophical remarks 66
 §1.6. Problems 67

Chapter 2. Abstract Groups and Group Presentations 70
 §2.1. Abstract groups 70
 §2.2. Morphisms of Groups 74
 §2.3. Subgroups 75
 §2.4. The Lagrange theorem 76
 §2.5. Quotient groups 77
 §2.6. Free groups and permutations 78
 §2.7. Group presentations 79
 §2.8. Cayley’s theorem 81
 §2.9. Problems 82

Chapter 3. Finite Subgroups of SO(3) and the Platonic Bodies 84
 §3.1. The Platonic bodies in art, philosophy, and science 85
 §3.2. Finite subgroups of SO(3) 87
 §3.3. The five regular polyhedra 94
 §3.4. The five Kepler cubes 95
 §3.5. Regular polyhedra in higher dimensions 96
 §3.6. Problems 98

Chapter 4. Discrete Subgroups of the Isometry Group of the Plane and Tilings 102
 §4.1. Tilings in architecture, art, and science 102
 §4.2. Tilings and crystallography 104
 §4.3. Isometries of the plane 106
 §4.4. Discrete groups and discrete geometries 107
 §4.5. The seventeen regular tilings 107
 §4.6. The 230 crystallographic groups 112
 §4.7. Problems 112

Chapter 5. Reﬂection Groups and Coxeter Geometries 116
 §5.1. An example: the kaleidoscope 116
 §5.2. Coxeter polygons and polyhedra 117
 §5.3. Coxeter geometries on the plane 118
 §5.4. Coxeter geometries in Euclidean space ℝ³ 120
 §5.5. Coxeter schemes and the classification theorem 122
 §5.6. Problems 124

Chapter 6. Spherical Geometry 126
 §6.1. A list of classical continuous geometries 126
 §6.2. Some basic facts from Euclidean plane geometry 130
 §6.3. Lines, distances, angles, polars, and perpendiculars 131
 §6.4. Biangles and triangles in S² 133
 §6.5. Other theorems about triangles 137
 §6.6. Coxeter triangles on the sphere S² 138
 §6.7. Two-dimensional elliptic geometry 138
 §6.8. Problems 140

Chapter 7. The Poincaré Disk Model of Hyperbolic Geometry 142
 §7.1. Inversion and orthogonal circles 143
 §7.2. Definition of the disk model 148
 §7.3. Points and lines in the hyperbolic plane 150
 §7.4. Perpendiculars 151
 §7.5. Parallels and nonintersecting lines 151
 §7.6. Sum of the angles of a triangle 152
 §7.7. Rotations and circles in the hyperbolic plane 153
 §7.8. Hyperbolic geometry and the physical world 155
 §7.9. Problems 156

Chapter 8. The Poincaré Half-Plane Model 160
 §8.1. Affine and linear-fractional transformations of C 161
 §8.2. The Poincaré half-plane model 164
 §8.3. Perpendiculars and parallels 165
 §8.4. Isometries w.r.t. Möbius distance 167
 §8.5. Problems 168

Chapter 9. The Cayley–Klein Model 170
 §9.1. Isometry and the Cayley–Klein model 170
 §9.2. Parallels in the Cayley–Klein model 173
 §9.3. Perpendiculars in the Cayley–Klein model 175
 §9.4. The hyperbolic line and relativity 176
 §9.5. Problems 177

Chapter 10. Hyperbolic Trigonometry and Absolute Constants 180
 §10.1. Isomorphism between the two disk models 180
 §10.2. Isomorphism between the two Poincaré models 185
 §10.3. Hyperbolic functions 186
 §10.4. Trigonometry on the hyperbolic plane 187
 §10.5. Angle of parallelism and Schweikart constant 187
 §10.6. Problems 190

Chapter 11. History of Non-Euclidean Geometry 194
 §11.1. Euclid’s Fifth Postulate 194
 §11.2. Statements equivalent to the Fifth Postulate 195
 §11.3. Gauss 196
 §11.4. Lobachevsky 197
 §11.5. Bolyai 199
 §11.6. Beltrami, Helmholtz, Lie, Cayley, Klein, Poincaré 200
 §11.7. Hilbert 201

Chapter 12. Projective Geometry 202
 §12.1. The projective plane as a geometry 202
 §12.2. Homogeneous coordinates 203
 §12.3. Projective transformations 205
 §12.4. Cross ratio of collinear points 208
 §12.5. Projective duality 209
 §12.6. Conics in ℝP² 211
 §12.7. The Desargues, Pappus, and Pascal theorems 211
 §12.8. Projective space ℝP³ 216
 §12.9. Problems 217

Chapter 13. “Projective Geometry Is All Geometry” 220
 §13.1. Subgeometries 220
 §13.2. The Euclidean plane as a subgeometry of ℝP² 221
 §13.3. The hyperbolic plane as a subgeometry of ℝP² 222
 §13.4. The elliptic plane as a subgeometry of ℝP² 224
 §13.5. Problems 226

Chapter 14. Finite Geometries 228
 §14.1. Small finite geometries 229
 §14.2. Finite fields 229
 §14.3. Example: the finite affine plane over F(5) 230
 §14.4. Example: the finite affine plane over F(2²) 232
 §14.5. Example of a finite projective plane 233
 §14.6. Axioms for finite affine planes 234
 §14.7. Axioms for finite projective planes 235
 §14.8. Constructing projective planes over finite fields 237
 §14.9. The Desargues theorem 238
 §14.10. Algebraic structures in finite projective planes 240
 §14.11. Open problems and conjectures 243
 §14.12. Problems 244

Chapter 15. The Hierarchy of Geometries 246
 §15.1. Dimension one: lines 247
 §15.2. Dimension two: planes 249
 §15.3. From metric to affine to projective 251
 §15.4. Three-dimensional space geometries 252
 §15.5. Finite and discrete geometries 253
 §15.6. The hierarchy of geometries 253
 §15.7. Problems 255

Chapter 16. Morphisms of Geometries 258
 §16.1. Examples of geometric covering spaces 259
 §16.2. Examples of geometric G-bundles 262
 §16.3. Lie groups 264
 §16.4. Examples of geometric vector bundles 265
 §16.5. Geometric G-bundles 267
 §16.6. The Milnor construction 268
 §16.7. Problems 269

Appendix A. Excerpts from Euclid’s “Elements” 272
 Postulates of Book I 273
 The Common Notions 274
 The Definitions of Book I 275
 The Propositions of Book I 279
 Conclusion 286

Appendix B. Hilbert’s Axioms for Plane Geometry 288
 I. Axioms of connection 289
 II. Axioms of order 291
 III. Axiom of parallels 292
 IV. Axioms of congruence 293
 V. Axiom of continuity 296
 Consistency of Hilbert’s axioms 297
 Conclusion 298

Answers and Hints 300
 Chapter 1 300
 Chapter 2 302
 Chapter 3 303
 Chapter 4 305
 Chapter 5 307
 Chapter 6 307
 Chapter 7 308
 Chapter 8 311
 Chapter 9 311
 Chapter 10 312
 Chapter 12 312
 Chapter 13 313
 Chapter 14 313
 Chapter 15 313
 Chapter 16 313

Bibliography 314
Index 316
