Preface 3
Contents 6

Chapter I. The Simplest Curves and Surfaces 9
 § 1. Plane Curves 9
 § 2. The Cylinder, the Cone, the Conic Sections and Their Surfaces of Revolution 15
 § 3. The Second-Order Surfaces 20
 § 4. The Thread Construction of the Ellipsoid, and Confocal Quadrics 27
 Appendices to Chapter I 33
  1. The Pedal-Point Construction of the Conies 33
  2. The Directrices of the Conies 35
  3. The Movable Rod Model of the Hyperboloid 37

Chapter II. Regular Systems of Points 40
 § 5. Plane Lattices 40
 § 6. Plane Lattices in the Theory of Numbers 45
 § 7. Lattices in Three and More than Three Dimensions 52
 § 8. Crystals as Regular Systems of Points 60
 § 9. Regular Systems of Points and Discontinuous Groups of Motions 64
 § 10. Plane Motions and their Composition; Classification of the Discontinuous Groups of Motions in the Plane 67
 § 11. The Discontinuous Groups of Plane Motions with Infinite Unit Cells 72
 § 12. The Crystallographic Groups of Motions in the Plane. Regular Systems of Points and Pointers. Division of the Plane into Congruent Cells 78
 § 13. Crystallographic Classes and Groups of Motions in Space. Groups and Systems of Points with Bilateral Symmetry 89
 § 14. The Regular Polyhedra 97

Chapter III. Projective Configurations 102
 § 15. Preliminary Remarks about Plane Configurations 103
 § 16. The Configurations (7₃) and (8₃) 106
 § 17. The Configurations (9₃) 110
 § 18. Perspective, Ideal Elements, and the Principle of Duality in the Plane 120
 § 19. Ideal Elements and the Principle of Duality in Space. Desargues' Theorem and the Desargues Configuration (6₃) 131
 § 20. Comparison of Pascal's and Desargues Theorems 136
 § 21. Preliminary Remarks on Configurations in Space 141
 § 22. Reye's Configuration 142
 § 23. Regular Polyhedra in Three and Four Dimensions, and their Projections 151
 § 24. Enumerative Methods of Geometry 165
 § 25. Schläfli's Double-Six 172

Chapter IV. Differential Geometry 179
 § 26. Plane Curves 180
 § 27. Space Curves 186
 § 28. Curvature of Surfaces. Elliptic, Hyperbolic, and Parabolic Points. Lines of Curvature and Asymptotic Lines. Umbilical Points, Minimal Surfaces, Monkey Saddles 191
 § 29. The Spherical Image and Gaussian Curvature 201
 § 30. Developable Surfaces, Ruled Surfaces 212
 § 31. The Twisting of Space Curves 219
 § 32. Eleven Properties of the Sphere 223
 § 33. Bendings Leaving a Surface Invariant 240
 § 34. Elliptic Geometry 243
 § 35. Hyperbolic Geometry, and its Relation to Euclidean and to Elliptic Geometry 250
 § 36. Stereographic Projection and Circle-Preserving Transformations. Poincare's Model of the Hyperbolic Plane 256
 § 37. Methods of Mapping, Isometric, Area-Preserving, Geodesic, Continuous and Conformal Mappings 268
 § 38. Geometrical Function Theory. Riemann's Mapping Theorem. Conformal Mapping in Space 271
 § 39. Conformal Mappings of Curved Surfaces. Minimal Surfaces. Plateau's Problem 276

Chapter V. Kinematics 280
 § 40. Linkages 280
 § 41. Continuous Rigid Motions of Plane Figures 283
 § 42. An Instrument for Constructing the Ellipse and its Roulettes 291
 § 43. Continuous Motions in Space 293

Chapter VI. Topology 297
 § 44. Polyhedra 298
 § 45. Surfaces 303
 § 46. One-Sided Surfaces 310
 § 47. The Projective Plane as a Closed Surface 321
 § 48. Standard Forms for the Surfaces of Finite Connectivity 329
 § 49. Topological Mappings of a Surface onto Itself. Fixed Points. Classes of Mappings. The Universal Covering Surface of the Torus 332
 § 50. Conformal Mapping of the Torus 338
 § 51. The Problem of Contiguous Regions, The Thread Problem, and the Color Problem 341
 Appendices to Chapter VI 348
  1. The Projective Plane in Four-Dimensional Space 348
  2. The Euclidean Plane in Four-Dimensional Space 349

Index 352
