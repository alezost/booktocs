Preface 4

VECTOR SPACES 8

1. First Introduction: Affine Geometry 8
 Vectors As Equivalence Classes of Arrows 9
 Addition and Scalar Multiplication 12
 Division Ratio 15
 Geometrical Problems via Vectors 16
  Desargues' Theorem 17
  Ceva's Theorem 19
  Coordinate System 22
  Intersection of Two Lines 24
  Intersection of a Line and a Plane 24
  Intersection of Two Planes 24
 Exercises 26
  Main Theorem of Proportions 27
  Theorem of Pappos and Pascal 27
  Menelaos' Theorem 28

2. Second Introduction: Linear Equations 29
 Example from Electrical Engineering 29
 Example from Economics 30
 Field 31
 Matrix 32
 Matrix Operations 33
 Types of Matrices 36
 Gaussian Algorithm 37
 Matrix Arithmetic 40
 Matrices As Linear Mappings 44
 Homogeneous and Inhomogeneous Systems of Linear Equations 47
 Exercises 49
  Magic Square 50
  Pythagorean Triplet 54

3. Vector Spaces 55
 Direct Product, Exterior Direct Sum 57
 Basic Linearity Definitions 60
 Bases 61
 Dimension, Rank, Formula for Subspaces 64
 Interior Direct Sum 65
 Quotient Space, Codimension 68
 Dimension Formula for Quotient Spaces 69
 Exercises 71
  Subspace Adapted to a Decomposition 76

4. Linear and Affine Mappings 78
 Linear Mapping, Affine Mapping 79
 Projection, Reflection 80
 Invariant Subspace 82
 Semisimple Endomorphism 83
 Monomorphism, Epimorphism, Isomorphism 86
 Image, Kernel and Cokernel 87
 Homomorphism Theorem 88
 Dimension Formula for Linear Mappings 89
 Vector Spaces of Linear Mappings 91
 Dual Space 92
 Exercises 94
  Involution 95
  Left and Right Inverse 97
  Annihilator, Evaluation Map 98

5. Abstract Affine Geometry 101
 Affine Space 101
 Affine Subspace, Codimension, Hyperplane 103
 General Position, Affine Mapping 106
 Convexity 107
 Topological Vector Space 109
 Norm 110
 Exercises 111
  Affine Coordinates 111
  Affine, Desarguesian and Papposian Planes 113
  Moulton Plane 113

6. Representation of Linear Mappings by Matrices 117
 Parameterization, Canonical Basis Isomorphism 117
 Matrix Representations of Linear Mappings 118
 Matrix Inverse 120
 Meaning of the Matrix Transpose 123
 Meaning of the Block Decomposition 124
 Change of Basis 125
 Equivalent Matrices, Rank 127
 Similar Matrices 130
 Nilpotent, Cyclic Subspace 130
 Exercises 136

7. Determinants 144
 Existence and Uniqueness of the Determinant 148
 Subdeterminant, Minor 151
 Properties of the Determinant 152
 Examples 153
  Vandermonde Determinant 154
 Adjunct of a Matrix 156
 Cramer's Rule 158
 Exercises 160

8. Volume Functions 165
 Motivation: Oriented Volumes 165
 Volume Function 166
 Symmetric Group, Transposition, Signature 167
 Existence and Uniqueness 168
 Formula for the Determinant 170
 Determinant As a Distortion Factor for Volumes 172
 Trace of an Endomorphism 174
 Basis Deformations, Orientation 176
 Exercises 179

9. Eigenvectors and Eigenvalues 183
 Eigenvector, Eigenvalue 183
 Eigenspace, Generalized Eigenspace 184
 Characteristic Polynomial 185
 Algorithm to Find Eigenvalues and Eigenvectors 187
 Algebraically Closed Field 189
 Gershgorin's Theorem 189
 Geometric and Algebraic Multiplicity 191
 Diagonalizability 192
 Simultaneous Diagonalizability 193
 Triagonalizability 194
 Simultaneous Triagonalizability 195
 Spectral Mapping Theorem 196
 Positive, Non-negative Matrices 199
 Perron-Frobenius Set 199
 Perron-Frobenius Theorem 200
 Perron-Frobenius Eigenvalue and Eigenvector 202
 Exercises 203
  Fibonacci Numbers 206
  Circulant Matrix 207

10. Classification of Endomorphisms up to Similarity 211
 Algebra, Polynomial 211
 Matrix Polynomial 214
 Hamilton-Cayley Theorem 215
 Minimal Polynomial 216
 Jordan Canonical Form 217
 Additive Jordan Decomposition 221
 Multiplicative Jordan Decomposition 222
 Frobenius' Theorem 223
 Determinantal Divisors 225
 Elementary Divisors 226
 Exercises 229
  Möbius Transformation 229
  Leverrier's Algorithm 230

11. Tensor Products and Base-Field Extensions 236
 Complexification 236
 Tensor Product of Vector Spaces 237
 Universal Property of the Tensor Product 238
 Matrix Representation of the Tensor Product 241
 Base-Field Extension 244
 Fitting Decomposition 246
 Characterization of Semisimplicity 247
 Galois Group of a Field Extension 249
 Jordan Decomposition over Not Algebraically Closed Field 249
 Exercises 251
  Complex Structure, Conjugate Space 251

12. Metric Geometry 255
 Cartesian Coordinate System, Scalar Product 255
 Projection 256
 Applications of Scalar Product in Geometry 258
  Oriented Area 261
  Skew-Symmetry 262
 Vector Product 264
 Applications of Vector Product in Geometry and Mechanics 266
  Torque, Angular Momentum 266
  Oriented Volume 266
  Identities with Scalar and Vector Products 268
  Spherical Trigonometry 269
 Exercises 272
  Differentiable Vector-Valued Function 276

13. Euclidean Spaces 277
 Scalar Product, Euclidean Space 277
 Complexification, Norm 278
 Orthogonality 280
 Orthogonal Complement 283
 Schmidt's Orthonormalization 285
 Riesz' Representation Theorem 286
 Gram's Determinant 287
 Normed Volume Functions 288
 Measure Theory 289
 Hadamard's Inequality 291
 Cavalieri's Principle 292
 Ptolemaios' Theorem 299
 Exercises 300
  Ptolemy Inequality 301
  Legendre Polynomials, Fourier Coefficients 303
  Bounded Set 304

14. Linear Mappings between Euclidean Spaces 307
 Adjoint of a Linear Mapping 307
 Properties 308
 Trace Norm 310
 Operator Norm 311
 Normal, Self-Adjoint, Skew-Adjoint, Unitary Endomorphisms 312
 Raleigh Quotient 318
 Positive Definite Mapping 319
 Pseudo-Inverse 322
 Kalman Filter, Gain Matrix 326
 Exercises 328

15. Bilinear Forms 335
 Bilinear and Sesquilinear Forms 335
 Representation by Matrices 336
 Congruence of Matrices 337
 Symmetric, Antisymmetric and Alternating Forms 339
 Orthogonality, Isotropy, Rank and Degeneracy 341
 Riessz' Representation Theorem 343
 Classification of Alternating Forms 345
 Pfaffian 346
 Existence of Orthogonal Bases 347
 Classification of Symmetric Forms 348
 Index and Signature of a Symmetric Form 350
 Quadratic Form, Quadric 351
 Sylvester's Law of Inertia 353
 Hyperbolic Plane and Pair 356
 Witt's Theorem 358
 Bruck-Chowla-Ryser Theorem 360
 Exercises 362
  Cartan-Killing Form, Hermitian Form 362

16. Groups of Automorphisms 370
 Linear Transformation Group, GL, SL 370
 Orthogonal and Unitary Groups 371
 Lorentz Group, Symplectic Group 373
 Rotation Group (SO) 375
 Group-Theoretical Interpretation of the Gaussian Algorithm 377
 Gaussian Decomposition of GL 378
 Bruhat Decomposition of GL 380
 Iwasawa, Polar and Cartan Decompositions 382
 Parameterizations of Groups 384
 Cayley Transform 386
 Exercises 388
  Stretching Endomorphism 395

17. Application: Markov Chains 397
 Markov Process, Transition Matrix and Graph 397
 Examples 398
 Stochastic Matrix, Probability Vector 400
 Communicating Classes 403
 Canonical Form of a Markov Process 404
 Fundamental Matrix of a Markov Process 405
 Regular Markov Chain 407
 Ergodic Markov Chain 409
 Cyclic Classes 411
 Mean First Passage Matrix 415
 Examples in Genetics 417
 Exercises 423
  Hardy-Weinberg Proportions 426

18. Application: Matrix Calculus and Differential Equations 428
 Uniqueness of DE Solution 430
 Wronskian Determinant 431
 Fundamental System 432
 Exponential Function for Matrices 434
 Higher-Order Differential Equations 439
 Variation of Constants 441
 Characteristic Polynomial 442
 Method of Judicious Guessing 444
 Fulmer's Algorithm to Exponentiate a Matrix 446
 Exercises 448
  Wronskian Determinant 452

GROUPS 455

19. Introduction: Symmetries of Geometric Figures 455
 Types of One- and Two-Dimensional Symmetries 458
 Exercises 464

20. Groups 465
 Semigroup, Monoid, Group 465
 Group Properties 466
 Group Table and Examples 467
  Integers 467
  Additive and Multiplicative Group of a Field 467
  Additive Group of a Vector Space 467
  Permutation Groups 467
  Symmetric Groups 468
  Dihedral Groups 468
  Automorphism Group of a Graph 469
  Matrix Groups 470
  Groups of Residue Classes 472
 Direct Product 474
 Exercises 476
  Left and Right Translations 476
  Conjugacy Classes 477

21. Subgroups and Cosets 480
 Subgroups 480
 Heisenberg Group 482
 Centralizer, Normalizer, Center 482
 Subgroup Lattice 483
 Cyclic Subgroup, Generator, Order 484
 Euler's Function 488
 Cosets 490
 Lagrange's Theorem 491
 Euler's and Fermat's Theorems 492
 Exercises 493
  Möbius Transformation 494
  Quaternions 495

22. Symmetric and Alternating Groups 501
 Cycle-Structure of a Permutation 502
 Permutation as a Product of Transpositions 504
 Even and Odd Permutations, Signature 507
 Alternating Group 507
 Exercises 509
  Josephus' Permutation 512

23. Group Homomorphisms 515
 Isomorphism, Automorphism 516
 Affine Group 517
 Conjugation, Inner and Outer Automorphisms 518
 Homomorphism 519
 Examples 520
  One-parameter Group, Lorentz Group 521
 Kernel, Image 522
 Cayley Embedding and Theorem 525
 Representation 526
 Exercises 527
  Spinor Representation of SL(2,ℂ) 532

24. Normal Subgroups and Factor Groups 533
 Characteristic and Normal Subgroups 533
 Quotient (Factor) Group 535
 Isomorphism Theorems 536
 Commutator Subroup, Abelianization 539
 Simple Group 540
 Semidirect Product 541
 Exercises 545
  Weyl Group 548
  Commutator Series 549
  Metaplectic Group 550

25. Free Groups; Generators and Relations 552
 Free Group 553
  Universal Property 554
 Generators and Relations 557
 Von Dyck's Theorem 557
 Free Abelian Groups 558
  Universal Property 559
 Linearly Independent and Generating Sets, Basis 559
 Direct Sum 560
 Rank of a Free Abelian Group 561
 Torsion Group 561
 Classification of Finitely Generated Abelian Groups 563
 Character of an Abelian Group 571
 Exercises 574

26. Group Actions 579
 Action, Natural Action 579
 Left-Regular Action 580
 Orbit, Stabilizer, Transitive Action 581
 Automorphism Group of a Graph 584
 Burnside's Lemma 585
 Cycle- and Pattern-Polynomial 590
 Polya's Theorem 591
 Applications to Colorings 592
 Exercises 595

27. Group-Theoretical Applications of Group Actions 600
 Double Cosets 600
 Class Equation 601
 Deriving Properties of a Finite Group from Its Order 601
 p-subgroup, p-Sylow-group 603
 Sylow's Theorems 604
  Applications 606
 Exercises 608

28. Nilpotent and Solvable Groups 612
 Commutator 612
 Commutator Series and Descending Central Series 612
  Examples 613
 Solvable Group, Nilpotent Group 617
 Ascending Central Series 619
 Characterization of Finite Nilpotent Groups 620
 Characterization of Solvable Transitive Subgroups of Symₚ 621
  Affine Permutation Group 621
  Classification Theorem 623
 Exercises 626

29. Topological Methods in Group Theory 628
 Topological Group 628
  Examples 629
  Properties 630
 Regularity and Homogeneity of a Topological Group 631
 Homomorphism of Topological Groups 632
 First Isomorphism Theorem (topological version) 634
 Вaire's Theorem 637
 Continuous Action, Open Mapping Theorem 637
 Closed Matrix Group 638
 Identity Component 639
 Examples 639
 Properties of Connected Topological Groups 642
 Krull Topology 642
 Exercises 645
  Character Group, Pontryagin's Duality Theorem 647

30. Analytical Methods in Group Theory 648
 Smooth Homomorphism 648
 Tangent Space, Dimension 649
 One-Parameter Subgroup 651
 Lie Object of a Matrix Group 652
 Lie Algebra 654
  Examples 654
 Relation between a Closed Matrix Group and Its Lie Algebra 658
 Adjoint Action 659
 Baker-Campbell-Hausdorff Formula 664
 Analytic Matrix Group and Its Lie Group Topology 666
 Exercises 670
  Algebraic Group 673

31. Groups in Topology 675
 Singular n-sphere, Homotopy 677
 Concatenation of Singular n-spheres 678
 Homotopy Groups, Fundamental Group 681
 Fixed Point Theorems 683
 Retraction 685
 Topological Monoid 686
 Commutativity of the Higher Homotopy Groups 686
 Homology Preview 687
 Standard and Singular n-simplices 689
 Boundary of a Singular n-simplex 690
 n-cycles, n-boundary, Homologous Cycles 691
 Homology Group 692
 Poincare's Theorem on Abelianization of a Fundamental Group 693
 Theory of Knots 696
  Knot Group 697
  Wirtinger's Algorithm and Presentation 697
  Examples 698
   Trefoil Knot 698
   Listing's Knot 698
 Exercises 701
  Deformation Retraction 701
  Compact-Open Topology 701
  Borromean Rings 704

Appendix 705
 A. Sets and Functions 705
 B. Relations 713
 C. Cardinal and Ordinal Numbers 722
 D. Point-Set Topology 732
 E. Categories and Functors 741

Bibliography 747
Index 749

Contents of Volume I 750
Contents of Volume II 770
