;;; al-toc.el --- Tools to convert TOC files into various formats

;; Copyright © 2019 Alex Kost

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.
;;
;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Code:

(require 'cl-lib)
(require 'dash)
(require 'bui)


;;; Text -> sexp -> any

(defvar al-toc/empty-line-regexp
  (rx bol (zero-or-more space) eol))

(defvar al-toc/line-regexp
  (rx string-start
      (group (zero-or-more space))
      (group (one-or-more any))
      (one-or-more space)
      (group (zero-or-one "-")
             (one-or-more digit))
      string-end))

(defvar al-toc/line-stack nil)
(defvar al-toc/line-stack-position 0)

(defun al-toc/end-of-buffer? ()
  "Return non-nil, if current position is the last in the current buffer."
  (= (point) (point-max)))

(defun al-toc/read-file-name (&optional prompt)
  "Like `read-file-name' but use the current file if in `dired-mode'."
  (read-file-name (or prompt "File: ") nil
                  (when (derived-mode-p 'dired-mode)
                    (dired-get-filename))))

(defun al-toc/parse-text-line (string)
  "Parse STRING as the TOC line.
Return nil, if it cannot be parsed.
Otherwise, return (LEVEL TITLE PAGE) list, where LEVEL is the
indentation level of this line."
  (and (string-match al-toc/line-regexp string)
       (list (length (match-string 1 string))
             (match-string 2 string)
             (string-to-number (match-string 3 string)))))

(defun al-toc/read-text-line ()
  "Read the current (or next) non-empty line.
See `al-toc/parse-text-line' for the returning value."
  (if (= 0 al-toc/line-stack-position)
      (progn
        (while (and (looking-at al-toc/empty-line-regexp)
                    (not (al-toc/end-of-buffer?)))
          (forward-line))
        (unless (al-toc/end-of-buffer?)
          (let* ((line (buffer-substring-no-properties
                        (line-beginning-position)
                        (line-end-position)))
                 (sexp (al-toc/parse-text-line line)))
            (and sexp
                 (push sexp al-toc/line-stack))
            (forward-line)
            sexp)))
    (setq al-toc/line-stack-position
          (1- al-toc/line-stack-position))
    (nth al-toc/line-stack-position al-toc/line-stack)))

(defun al-toc/unread-text-line ()
  "Forget the last line, read by `al-toc/read-text-line'."
  (setq al-toc/line-stack-position
        (1+ al-toc/line-stack-position)))

(defun al-toc/read-text-buffer (&optional buffer)
  "Return sexp with TOC from BUFFER with TOC in text format."
  (setq al-toc/line-stack nil
        al-toc/line-stack-position 0)
  (with-current-buffer (or buffer (current-buffer))
    (goto-char (point-min))
    (al-toc/read-text-buffer-1 0)))

(defun al-toc/read-text-buffer-1 (level)
  "Subroutine of `al-toc/read-text-buffer'."
  (let (done sexp res)
    (while (not done)
      (let ((line (al-toc/read-text-line)))
        (if line
            (cl-multiple-value-bind (new-level title page)
                line
              (cond
               ((= new-level level)
                (setq res (unless (and (null res) (null sexp))
                            (cons sexp res))
                      sexp (list title page)))
               ((> new-level level)
                (al-toc/unread-text-line)
                (setq sexp
                      (append sexp
                              (al-toc/read-text-buffer-1 new-level))))
               (t
                (al-toc/unread-text-line)
                (setq done t))))
          (setq done t))))
    (nreverse (cons sexp res))))

(defun al-toc/read-text-file (file)
  "Return sexp with TOC from FILE with TOC in text format."
  (with-temp-buffer
    (insert-file-contents file)
    (al-toc/read-text-buffer)))

(defun al-toc/my-sexp->djvu-sexp (sexp)
  "Return sexp in DJVU TOC format from SEXP in my TOC format."
  (cons 'bookmarks
        (-tree-map (lambda (elt)
                     (if (numberp elt)
                         (concat "#" (number-to-string elt))
                       elt))
                   sexp)))

(defun al-toc/print-sexp-to-file (sexp file)
  "Write prettified SEXP to FILE."
  (with-temp-file file
    (prin1 sexp (current-buffer))
    (goto-char (point-max))
    (insert "\n")
    (pp-buffer)))

;;;###autoload
(defun al-toc/text-to-my-toc-file (in-file &optional out-file)
  "Convert IN-FILE in text TOC format to OUT-FILE in my sexp TOC format.
OUT-FILE is the file name of the output file.  If it is nil, the
name of IN-FILE will be used with '.el' extension.
Return OUT-FILE."
  (interactive (list (al-toc/read-file-name "Text file: ")))
  (let ((out-file (or out-file
                      (concat (file-name-sans-extension in-file)
                              ".el")))
        (sexp (al-toc/read-text-file in-file)))
    (al-toc/print-sexp-to-file sexp out-file)
    (message "Written file with TOC: %s" out-file)
    out-file))

;;;###autoload
(defun al-toc/text-to-djvu-toc-file (in-file &optional out-file)
  "Convert IN-FILE in text TOC format to OUT-FILE in DJVU outline format.
OUT-FILE is the file name of the output file.  If it is nil, the
name of IN-FILE will be used with '.djvu.el' extension.
Return OUT-FILE."
  (interactive (list (al-toc/read-file-name "Text file: ")))
  (let ((out-file (or out-file
                      (concat (file-name-sans-extension in-file)
                              ".djvu.el")))
        (sexp (al-toc/my-sexp->djvu-sexp
               (al-toc/read-text-file in-file))))
    (al-toc/print-sexp-to-file sexp out-file)
    (message "Written file with DJVU outline: %s" out-file)
    out-file))

(defun al-toc/string->pdf-utf (string)
  "Convert STRING to UTF-coded string used in PDF."
  (concat "<FEFF"
          (mapconcat (lambda (char) (format "%04x" char))
                     (string-to-list string)
                     "")
          ">"))

(defun al-toc/print-sexp-as-ps (sexp)
  "Write SEXP with TOC in my format as postscript to the current buffer."
  (dolist (list sexp)
    (pcase list
      (`(,title ,page . ,subtoc)
       (insert "[/Page " (number-to-string page)
               " /Title " (al-toc/string->pdf-utf title))
       (when subtoc
         (insert " /Count " (number-to-string (length subtoc))))
       (insert " /OUT pdfmark\n")
       (when subtoc
         (al-toc/print-sexp-as-ps subtoc))))))

(defun al-toc/sexp-to-ps-file (sexp out-file)
  "Write SEXP with TOC in my format to OUT-FILE in postscript."
  (with-temp-file out-file
    (al-toc/print-sexp-as-ps sexp)))

;;;###autoload
(defun al-toc/text-to-pdf-toc-file (in-file &optional out-file)
  "Convert IN-FILE in text TOC format to OUT-FILE in PDF outline format.
OUT-FILE is the file name of the output file.  If it is nil, the
name of IN-FILE will be used with '.ps' extension.
Return OUT-FILE."
  (interactive (list (al-toc/read-file-name "Text file: ")))
  (let ((out-file (or out-file
                      (concat (file-name-sans-extension in-file)
                              ".ps")))
        (sexp (al-toc/read-text-file in-file)))
    (al-toc/sexp-to-ps-file sexp out-file)
    (message "Written file with PDF outline: %s" out-file)
    out-file))

;;;###autoload
(defun al-toc/text-to-all-files (in-file)
  "Convert IN-FILE in text format to files in other TOC formats.
The name of IN-FILE will be used for the other files with the
appropriate extensions."
  (interactive (list (al-toc/read-file-name "Text file: ")))
  (let ((file-name (file-name-sans-extension in-file))
        (sexp (al-toc/read-text-file in-file)))
    (al-toc/print-sexp-to-file sexp (concat file-name ".el"))
    (al-toc/print-sexp-to-file (al-toc/my-sexp->djvu-sexp sexp)
                               (concat file-name ".djvu.el"))
    (al-toc/sexp-to-ps-file sexp (concat file-name ".ps"))))


;;; djvu -> text

(defun al-toc/read-djvu-toc-buffer (&optional buffer)
  "Return sexp with TOC in djvu format from BUFFER."
  (or buffer (setq buffer (current-buffer)))
  (with-current-buffer buffer
    (goto-char (point-min))
    (read buffer)))

(defun al-toc/read-djvu-toc-file (file)
  "Return sexp with TOC from FILE with TOC in djvu format."
  (with-temp-buffer
    (insert-file-contents file)
    (al-toc/read-djvu-toc-buffer)))

(defun al-toc/print-djvu-sexp-to-buffer (sexp)
  "Write SEXP with TOC in djvu format to the current buffer."
  (dolist (entry sexp)
    (pcase entry
      (`(,title ,page . ,rest)
       ;; Excluding leading "#" from PAGE.
       (insert title " " (substring page 1) "\n")
       (when rest
         (bui-with-indent 1
           (al-toc/print-djvu-sexp-to-buffer rest)))))))

(defun al-toc/print-djvu-sexp-to-text-file (sexp file)
  "Write SEXP with TOC in djvu format to text FILE."
  (with-temp-file file
    ;; Excluding leading 'bookmarks'.
    (al-toc/print-djvu-sexp-to-buffer (cdr sexp))))

;;;###autoload
(defun al-toc/djvu-to-text-toc-file (in-file &optional out-file)
  "Convert IN-FILE in DJVU outline format to OUT-FILE in text format.
OUT-FILE is the file name of the output file.  If it is nil, the
name of IN-FILE will be used with '.txt' extension.
Return OUT-FILE."
  (interactive (list (al-toc/read-file-name "Djvu TOC file: ")))
  (let ((out-file (or out-file
                      (concat (file-name-sans-extension in-file)
                              ".txt")))
        (sexp (al-toc/read-djvu-toc-file in-file)))
    (al-toc/print-djvu-sexp-to-text-file sexp out-file)
    (message "Written text file with TOC: %s" out-file)
    out-file))

(provide 'al-toc)

;;; al-toc.el ends here
